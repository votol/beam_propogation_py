#!/usr/bin/env python

import numpy
import math
import cmath
import copy


class Grid:
    def __init__(self, lenx, xmin, xmax, leny, ymin, ymax):
        self.number_of_x_elements = lenx
        self.minimum_x = xmin
        self.maximum_x = xmax
        self.number_of_y_elements = leny
        self.minimum_y = ymin
        self.maximum_y = ymax

    def __init__(self, lenx, x, leny, y):
        self.number_of_x_elements = lenx
        self.minimum_x = -x
        self.maximum_x = x
        self.number_of_y_elements = leny
        self.minimum_y = -y
        self.maximum_y = y


class FunctionInitializer:
    def __init__(self, function):
        self.initializer_function = function

    def initialize(self, grid_impl):
        delta_x = (grid_impl.maximum_x - grid_impl.minimum_x) / grid_impl.number_of_x_elements
        delta_y = (grid_impl.maximum_y - grid_impl.minimum_y) / grid_impl.number_of_y_elements
        matrix = numpy.zeros((grid_impl.number_of_y_elements, grid_impl.number_of_x_elements), dtype=complex)
        x = grid_impl.minimum_x
        for n in range(0, grid_impl.number_of_x_elements):
            y = grid_impl.minimum_y
            for m in range(0, grid_impl.number_of_y_elements):
                matrix[m, n] = self.initializer_function(x, y)
                y += delta_y
            x += delta_x
        return matrix


class FourierInitializer:
    def __init__(self, alph, input_field):
        self.alph = alph
        self.input_field = input_field

    def initialize(self, grid_impl):
        dx_in = (self.input_field.grid.maximum_x -
                 self.input_field.grid.minimum_x) / self.input_field.grid.number_of_x_elements
        dx_out = (grid_impl.maximum_x - grid_impl.minimum_x) / grid_impl.number_of_x_elements
        T = numpy.zeros((self.input_field.grid.number_of_x_elements, grid_impl.number_of_x_elements), dtype=complex)
        x_out = grid_impl.minimum_x
        for n in range(0, grid_impl.number_of_x_elements):
            x_in = self.input_field.grid.minimum_x
            for m in range(0, self.input_field.grid.number_of_x_elements):
                T[m, n] = cmath.exp(1j*self.alph*x_in*x_out)*dx_in
                x_in += dx_in
            x_out += dx_out
        matrix = numpy.dot(self.input_field.field_matrix, T)
        dy_in = (self.input_field.grid.maximum_y -
                 self.input_field.grid.minimum_y) / self.input_field.grid.number_of_y_elements
        dy_out = (grid_impl.maximum_y - grid_impl.minimum_y) / grid_impl.number_of_y_elements
        T = numpy.zeros((grid_impl.number_of_y_elements, self.input_field.grid.number_of_y_elements), dtype=complex)
        y_out = grid_impl.minimum_y
        for n in range(0, grid_impl.number_of_y_elements):
            y_in = self.input_field.grid.minimum_y
            for m in range(0, self.input_field.grid.number_of_y_elements):
                T[n, m] = cmath.exp(1j * self.alph * y_in * y_out) * dy_in
                y_in += dy_in
            y_out += dy_out
        return numpy.dot(T, matrix)*self.alph/(2 * cmath.pi)


class KerrPhaseShifter:
    def __init__(self, phase):
        self.phase = phase

    def get_matrix(self, field_distribution_impl):
        intensity = field_distribution_impl.abs()
        return intensity * self.phase / intensity.max()

class ParabolicPhaseShifter:
    def __init__(self, parameter):
        self.parameter = parameter

    def get_matrix(self, field_distribution_impl):
        grid = field_distribution_impl.grid
        delta_x = (grid.maximum_x - grid.minimum_x) / grid.number_of_x_elements
        delta_y = (grid.maximum_y - grid.minimum_y) / grid.number_of_y_elements
        matrix = numpy.zeros((grid.number_of_y_elements, grid.number_of_x_elements), dtype=complex)
        x = grid.minimum_x
        for n in range(0, grid.number_of_x_elements):
            y = grid.minimum_y
            for m in range(0, grid.number_of_y_elements):
                matrix[m, n] = x * x  + y * y
                y += delta_y
            x += delta_x
        return self.parameter * matrix

class FieldDistribution:
    def __init__(self, grid_impl, initializer):
        self.grid = copy.deepcopy(grid_impl)
        self.field_matrix = initializer.initialize(self.grid)

    def phase_shift(self, shifter):
        self.field_matrix *= numpy.exp(1j * shifter.get_matrix(self))

    def abs(self):
        return (self.field_matrix.conjugate()*self.field_matrix).real

    def width(self):
        intensity = (self.field_matrix.conjugate()*self.field_matrix).real
        delta_x = (self.grid.maximum_x - self.grid.minimum_x) / self.grid.number_of_x_elements
        x_array = numpy.zeros(self.grid.number_of_x_elements)
        x = self.grid.minimum_x
        for n in range(0, self.grid.number_of_x_elements):
            x_array[n] = x
            x += delta_x
        mean_x = numpy.sum(numpy.outer(
            numpy.ones(self.grid.number_of_y_elements), x_array)*intensity)/numpy.sum(intensity)
        delta_y = (self.grid.maximum_y - self.grid.minimum_y) / self.grid.number_of_y_elements
        y_array = numpy.zeros(self.grid.number_of_y_elements)
        y = self.grid.minimum_y
        for n in range(0, self.grid.number_of_y_elements):
            y_array[n] = y
            y += delta_y
        mean_y = numpy.sum(numpy.outer(y_array,
            numpy.ones(self.grid.number_of_x_elements)) * intensity) / numpy.sum(intensity)
        matrix = numpy.zeros((self.grid.number_of_y_elements, self.grid.number_of_x_elements), dtype=complex)
        x = self.grid.minimum_x
        for n in range(0, self.grid.number_of_x_elements):
            y = self.grid.minimum_y
            for m in range(0, self.grid.number_of_y_elements):
                matrix[m, n] = (x-mean_x)*(x-mean_x) + (y-mean_y)*(y-mean_y)
                y += delta_y
            x += delta_x
        return cmath.sqrt(numpy.sum(matrix*intensity)/numpy.sum(intensity)).real

    def get_intensity(self, x, y):
        return (self.field_matrix[y, x].conjugate()*self.field_matrix[y, x]).real

    def get_x_slice_intensity(self, y):
        result = numpy.zeros((2, self.grid.number_of_x_elements), dtype=float)
        result[1, :] = (self.field_matrix[y, :] * self.field_matrix.conjugate()[y, :]).real
        delta_x = (self.grid.maximum_x - self.grid.minimum_x) / self.grid.number_of_x_elements
        x = self.grid.minimum_x
        for n in range(0, self.grid.number_of_x_elements):
            result[0, n] = x
            x += delta_x
        return result

    def get_y_slice_intensity(self, x):
        result = numpy.zeros((2, self.grid.number_of_y_elements), dtype=float)
        result[1, :] = (self.field_matrix[:, x] * self.field_matrix.conjugate()[:, x]).real
        delta_y = (self.grid.maximum_y - self.grid.minimum_y) / self.grid.number_of_y_elements
        y = self.grid.minimum_y
        for n in range(0, self.grid.number_of_y_elements):
            result[0, n] = y
            y += delta_y
        return result

    def get_x_slice_phase(self, y):
        result = numpy.zeros((2, self.grid.number_of_x_elements), dtype=float)
        result[1, :] = numpy.angle(self.field_matrix[y, :])
        delta_x = (self.grid.maximum_x - self.grid.minimum_x) / self.grid.number_of_x_elements
        x = self.grid.minimum_x
        for n in range(0, self.grid.number_of_x_elements):
            result[0, n] = x
            x += delta_x
        return result

    def get_y_slice_phase(self, x):
        result = numpy.zeros((2, self.grid.number_of_y_elements), dtype=float)
        result[1, :] = numpy.angle(self.field_matrix[:, x])
        delta_y = (self.grid.maximum_y - self.grid.minimum_y) / self.grid.number_of_y_elements
        y = self.grid.minimum_y
        for n in range(0, self.grid.number_of_y_elements):
            result[0, n] = y
            y += delta_y
        return result

if __name__ == "__main__":
    import matplotlib.pyplot as plt

    print('testing...')

    F = 40
    delta = -1.0
    lambd = 8.0 * 1e-5;
    k = 2 * cmath.pi / lambd;
    w_in = 0.6
    w_F = F / (k * w_in);
    w_F = w_F * math.sqrt(1 + 4*delta*delta/(k*k*w_F*w_F*w_F*w_F))
    matrix_dim_in = 2048
    matrix_dim_out = 1024

    def func(x, y):
        return cmath.exp(-(x*x+y*y)/(2*w_in*w_in)-1j*((x*x+y*y)/(1.224*w_in*w_in))**2)

    a = FieldDistribution(Grid(matrix_dim_in, w_in*5, matrix_dim_in, w_in*5), FunctionInitializer(func))
    a.phase_shift(ParabolicPhaseShifter(k * delta / (2 * F * (F - delta))))
    b = FieldDistribution(Grid(matrix_dim_in, w_F*20, matrix_dim_in, w_F*20), FourierInitializer(k / (F - delta), a))
    #b.phase_shift(KerrPhaseShifter(3.14))
    #b.phase_shift(ParabolicPhaseShifter(k*delta/(2*F*(F - delta))))
    c = FieldDistribution(Grid(matrix_dim_out, w_in * 5, matrix_dim_out, w_in * 5), FourierInitializer(k / (F - delta), b))
    plt.matshow(a.abs(), cmap=plt.cm.gray)
    plt.matshow(b.abs(), cmap=plt.cm.gray)
    plt.matshow(c.abs(), cmap=plt.cm.gray)
    c_slice = c.get_y_slice_phase(int(matrix_dim_out/2))
    plt.figure(4)
    plt.plot(c_slice[0, :], c_slice[1, :])
    plt.show()

    print(c.get_intensity(int(matrix_dim_out/2), int(matrix_dim_out/2)))